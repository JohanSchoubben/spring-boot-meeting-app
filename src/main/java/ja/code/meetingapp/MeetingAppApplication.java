package ja.code.meetingapp;

import ja.code.meetingapp.model.Event;
import ja.code.meetingapp.service.EventService;
import ja.code.meetingapp.service.EventServiceImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.cache.CacheAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;

import java.time.LocalDateTime;
import java.util.List;

@SpringBootApplication
public class MeetingAppApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(MeetingAppApplication.class, args);
        EventService eventService = context.getBean(EventServiceImpl.class);
        List<Event> events = eventService.findFutureEvents();
        events.forEach(System.out::println);
    }

}
