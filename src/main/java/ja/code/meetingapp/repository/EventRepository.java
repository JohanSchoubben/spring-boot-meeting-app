package ja.code.meetingapp.repository;

import ja.code.meetingapp.model.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface EventRepository extends JpaRepository<Event, Long> {

    List<Event> findEventsByEndTimestampLessThan(LocalDateTime time);
    List<Event> findEventsByStartTimestampGreaterThan(LocalDateTime time);
}
