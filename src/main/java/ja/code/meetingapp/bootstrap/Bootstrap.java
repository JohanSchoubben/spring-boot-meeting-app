package ja.code.meetingapp.bootstrap;

import ja.code.meetingapp.model.Event;
import ja.code.meetingapp.model.User;
import ja.code.meetingapp.service.EventService;
import ja.code.meetingapp.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.Set;

@Component
@AllArgsConstructor
@Profile("test")
public class Bootstrap {

    private UserService userService;
    private EventService eventService;

    @PostConstruct
    public void init() {
        User user = User.builder()
                .firstName("Abdulvahap")
                .lastName("Karabekmez")
                .dob(LocalDate.of(1984, Month.MAY, 15))
                .email("vahap@mail.com")
                .password("password")
                .role("member").build();
        userService.register(user);
        User user1 = User.builder()
                .firstName("Johan")
                .lastName("Schoubben")
                .dob(LocalDate.of(1967, Month.JANUARY, 10))
                .email("johan@gmail.be")
                .password("bling")
                .role("member").build();
        userService.register(user1);

        Event ouderContact = Event.builder()
                .title("Oudercontract")
                .subject("Last meeting")
                .description("Eind evaluatie van schooljaar")
                .startTimestamp(LocalDateTime.of(2021, Month.MAY, 28, 17, 30))
                .endTimestamp(LocalDateTime.of(2021, Month.MAY, 28, 18, 30))
                .location("Genk")
                .attendees(Set.of(user1, user))
                .build();
        eventService.register(ouderContact);
        Event jobDay = Event.builder()
                .title("JobDay")
                .subject("Ontmoeting bedrijven")
                .description("voorstelling bedrijven en studenten")
                .startTimestamp(LocalDateTime.of(2021, Month.JUNE, 15, 9, 0))
                .endTimestamp(LocalDateTime.of(2021, Month.JUNE, 15, 12, 30))
                .location("Genk")
                .attendees(Set.of(user1, user))
                .build();
        eventService.register(jobDay);
    }
}
