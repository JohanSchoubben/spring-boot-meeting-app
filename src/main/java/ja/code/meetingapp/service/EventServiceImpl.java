package ja.code.meetingapp.service;

import ja.code.meetingapp.model.Event;
import ja.code.meetingapp.repository.EventRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class EventServiceImpl implements EventService {

    private final EventRepository eventRepository;
    @Override
    public Event register(Event event) {
        return eventRepository.save(event);
    }

    @Override
    public List<Event> findAllEvents() {
        return eventRepository.findAll();
    }

    @Override
    public List<Event> findPastEvents() {
        return eventRepository.findEventsByEndTimestampLessThan(LocalDateTime.now());
    }

    @Override
    public List<Event> findFutureEvents() {
        return eventRepository.findEventsByStartTimestampGreaterThan(LocalDateTime.now());
    }
}
