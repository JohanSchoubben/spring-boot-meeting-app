package ja.code.meetingapp.service;

import ja.code.meetingapp.model.User;


public interface UserService {

    User register(User user);
    User findUserById(Long id);
}
