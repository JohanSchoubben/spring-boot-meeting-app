package ja.code.meetingapp.service;

import ja.code.meetingapp.model.Event;
import java.util.List;

public interface EventService {
    Event register(Event event);
    List<Event> findAllEvents();
    List<Event> findPastEvents();
    List<Event> findFutureEvents();

}
